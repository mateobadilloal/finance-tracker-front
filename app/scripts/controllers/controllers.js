'use strict';

angular.module('financeTracker')

.controller('headerController', ['$scope', '$rootScope', '$state', 'AuthFactory', 'cookieFactory', function($scope, $rootScope, $state, AuthFactory, cookieFactory) {

        $scope.stateis = function(curstate) {
            return $state.is(curstate);
        };

        //logs out the user destroying the cookies with the user credentials and sesion token
        $scope.logOut = function() {
            AuthFactory.setValidUser(false);
            $scope.loggedIn = AuthFactory.isValidUser();
            cookieFactory.unsetcookie("_st");
            cookieFactory.unsetcookie("_ui");
        };
        if (cookieFactory.getcookie('_st') != undefined) {
            AuthFactory.setValidUser(true);
        }
        $scope.loggedIn = AuthFactory.isValidUser();

    }])
    .controller('landingController', ['$scope', function($scope) {
        $scope = 3;
    }])
    .controller('footerController', ['$scope', function($scope) {

        $scope = 3;
    }])
    .controller('currencyController', ['$scope', 'currencyFactory', '$timeout', function($scope, currencyFactory, $timeout) {

        var cunrrenciesNamedArray = [];
        $scope.currencies = [];

        //Array of currencies that are available to convert
        $scope.coins = [
            "Choose Currency",
            "USD",
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            "IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            "PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "ZAR",
            "EUR"
        ];

        $scope.currency = {
            fromAmount: 0,
            toAmount: 0
        };


        //Every time the view is loaded it gets the name of the popular currencies from the database with getNamedCurs() then it goes to the fixer.io API to get the rates between the currencies with the methos fixerCurs()
        currencyFactory.getNamedCurs().query().
        $promise.then(
            function(response) {
                for (var i = 0; i < response.length; i++) {
                    cunrrenciesNamedArray.push(response[i]);
                }
            },
            function(response) {
                console.log(response);
            });

        $timeout(function() {

            for (var i = 0; i < cunrrenciesNamedArray.length; i++) {
                currencyFactory.fixerCurs().get({
                    origin: cunrrenciesNamedArray[i].origin,
                    versus: cunrrenciesNamedArray[i].versus
                }).$promise.then(
                    function(response) {
                        var name = Object.keys(response.rates)[0] + '/' + response.base;
                        $scope.currencies.push({
                            name: name,
                            rates: response.rates[Object.keys(response.rates)[0]]
                        });

                    },
                    function(response) {
                        console.log(response);
                    }
                );
            }
        }, 500);

        //This method converts from one currency to another using the fixer.io API
        $scope.convert = function() {
            currencyFactory.fixerCurs().get({
                origin: $scope.currency.from,
                versus: $scope.currency.to
            }).$promise.then(
                function(response) {
                    var rate = response.rates[Object.keys(response.rates)[0]];
                    $scope.currency.toAmount = $scope.currency.fromAmount * rate;
                },
                function(response) {
                    console.log(response);
                }
            );

        };






    }])
    .controller('glossaryController', ['$scope', 'glossaryFactory', function($scope, glossaryFactory) {

        $scope.tips = [];
        $scope.formula = '';
        //This controller gets the tips from the database to be shown in the financial glossary view
        glossaryFactory.getTips().query()
            .$promise.then(
                function(response) {
                    $scope.tips = response;
                    $scope.formula = $scope.tips[5].description;
                },
                function(response) {
                    console.log(response);
                }
            );


    }])
    .controller('dashboardController', ['$scope', '$state', 'AuthFactory', '$mdToast', 'accountFactory', 'cookieFactory', function($scope, $state, AuthFactory, $mdToast, accountFactory, cookieFactory) {

        $scope.accounts = [];
        $scope.ui = cookieFactory.getcookie('_ui');

        //This method validates if the user is logged in with the Authfactory.validatePath(), if not redirects to home page
        if (!AuthFactory.validatePath('landing.dashboard.tracker')) {
            $mdToast.show(
                $mdToast.simple()
                .textContent('You must be logged in!')
                .position('top right')
                .theme('info-toast')
                .hideDelay(3000)
            );

        }


        $scope.stateis = function(curstate) {
            return $state.is(curstate);
        };


        //gets accounts of the actual user
        accountFactory.getAccounts().query()
            .$promise.then(
                function(response) {
                    $scope.accounts = response;

                },
                function(response) {
                    console.log(response);
                });

    }])
    .controller('trackerController', ['$scope', 'trackerFactory', '$timeout', '$state', function($scope, trackerFactory, $timeout, $state) {
        $scope.tracker = {
            type: "default"
        };
        $scope.trackersArr = [];



        $scope.data = [{
                "key": "-",
                "values": []
            }

        ];

        //This method is used if the account select in the dashboard.tracker changes account and brings the information of the selectec account 
        // to show the graph using nvd3 stackedAreaChart
        $scope.accountChange = function() {

            $scope.trackersArr = [];
            trackerFactory.getTrackers($scope.tracker.account.id).query().$promise.then(
                function(response) {
                    for (var i = 0; i < response.length; i++) {
                        var tempArr = [response[i].date, response[i].amount];
                        $scope.trackersArr.push(tempArr);
                    }

                    $scope.options = {
                        chart: {
                            type: 'stackedAreaChart',
                            height: 450,
                            margin: {
                                top: 20,
                                right: 20,
                                bottom: 30,
                                left: 40
                            },
                            x: function(d) {
                                return d[0];
                            },
                            y: function(d) {
                                return d[1];
                            },
                            useVoronoi: false,
                            clipEdge: true,
                            duration: 100,
                            useInteractiveGuideline: true,
                            xAxis: {
                                showMaxMin: false,
                                tickFormat: function(d) {
                                    return d3.time.format('%x')(new Date(d));
                                }
                            },
                            yAxis: {
                                tickFormat: function(d) {
                                    return d3.format(',.2f')(d);
                                }
                            },
                            zoom: {
                                enabled: true,
                                scaleExtent: [1, 10],
                                useFixedDomain: false,
                                useNiceScale: false,
                                horizontalOff: false,
                                verticalOff: true,
                                unzoomEventType: 'dblclick.zoom'
                            },
                            showControls: false,
                            refreshDataOnly: true
                        }
                    };
                    $scope.data = [{
                            "key": $scope.tracker.account.name,
                            "values": $scope.trackersArr
                        }

                    ];
                },
                function(response) {
                    console.log(response);

                });

        };

        // This methos creates a new tracker for the selected account
        $scope.addTrack = function() {
            var date = new Date($scope.tracker.date).getTime();
            var type = false;
            var amount = 0;
            var firstTime = false;
            if ($scope.trackersArr.length == 0) {
                amount = $scope.tracker.amount;
                firstTime = true;
            } else {
                amount = $scope.trackersArr[$scope.trackersArr.length - 1][1] + $scope.tracker.amount;

            }


            if ($scope.tracker.type == 2) {
                type = false;
                amount = amount * -1;
            } else
            if ($scope.tracker.type == 1) {
                type = true;
            }

            trackerFactory.postTrack().save({
                income: type,
                amount: amount,
                date: date,
                customerId: $scope.ui,
                accountsId: $scope.tracker.account.id

            }).$promise.then(
                function(response) {
                    if (firstTime == true) {
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    } else {
                        $scope.accountChange();
                    }

                },
                function(response) {
                    console.log(response);
                });
        };
    }])
    .controller('accountsController', ['$scope', 'ngDialog', 'accountFactory', '$state', function($scope, ngDialog, accountFactory, $state) {

        //Using ng-dialog the registration and deletion of accounts was done
        $scope.openRegisterAc = function() {
            ngDialog.open({
                template: '../../views/registerAccount.html',
                scope: $scope,
                className: 'ngdialog-theme-default',
                controller: "accountsController"
            });
        };

        $scope.openEditAc = function() {
            ngDialog.open({
                template: '../../views/editAccount.html',
                scope: $scope,
                className: 'ngdialog-theme-default',
                controller: "accountsController"
            });
        };

        $scope.openDeleteAc = function() {
            ngDialog.open({
                template: '../../views/deleteAccount.html',
                scope: $scope,
                className: 'ngdialog-theme-default',
                controller: "accountsController"
            });
        };


        $scope.registerAccount = function() {
            accountFactory.registerAccount().save({
                    name: $scope.regAcc.name,
                    description: $scope.regAcc.description,
                    customerId: $scope.ui

                })
                .$promise.then(
                    function(response) {
                        ngDialog.close();
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    },
                    function(response) {
                        console.log(response);
                    });

        };

        $scope.deleteAccount = function() {
            var account = $scope.delAcc.name;
            accountFactory.deleteAccount().delete({
                    id: account.id
                })
                .$promise.then(
                    function(response) {
                        ngDialog.close();
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    },
                    function(response) {
                        console.log(response);
                    });
        };



    }])
    .controller('registerController', ['$scope', 'AuthFactory', '$mdToast', '$state', 'cookieFactory', function($scope, AuthFactory, $mdToast, $state, cookieFactory) {

        // Registration of an user controller
        $scope.register = function() {
            AuthFactory.register().save({
                email: $scope.register.email,
                username: $scope.register.username,
                password: $scope.register.password
            }).$promise.then(
                function(response) {
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Registration Successful!')
                        .position('top right')
                        .theme('success-toast')
                        .hideDelay(3000)
                    );

                    AuthFactory.login().save({
                        username: $scope.register.username,
                        password: $scope.register.password
                    }).$promise.then(
                        function(response) {
                            var today = new Date();
                            var expiresValue = new Date(today);
                            AuthFactory.setValidUser(true);
                            expiresValue.setSeconds(today.getSeconds() + response.ttl);
                            $state.go('landing.dashboard.tracker');
                            cookieFactory.setcookie("_st", response.id, {
                                'expires': expiresValue
                            });
                            cookieFactory.setcookie("_ui", response.userId, {
                                'expires': expiresValue
                            });

                        },
                        function(response) {
                            $mdToast.show(
                                $mdToast.simple()
                                .textContent('Login Error!')
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(3000)
                            );
                        }
                    );

                },
                function(response) {
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Registreation Error!')
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(3000)
                    );
                }
            );
        };
    }])
    .controller('loginController', ['$scope', '$rootScope', 'AuthFactory', '$mdToast', '$state', 'cookieFactory', function($scope, $rootScope, AuthFactory, $mdToast, $state, cookieFactory) {

        //User login controller
        $scope.logIn = function() {

            AuthFactory.login().save({
                username: $scope.login.username,
                password: $scope.login.password
            }).$promise.then(
                function(response) {

                    var today = new Date();
                    var expiresValue = new Date(today);
                    AuthFactory.setValidUser(true);
                    expiresValue.setSeconds(today.getSeconds() + response.ttl);

                    $state.go('landing.dashboard.tracker');
                    cookieFactory.setcookie("_st", response.id, {
                        'expires': expiresValue
                    });
                    cookieFactory.setcookie("_ui", response.userId, {
                        'expires': expiresValue
                    });

                },
                function(response) {
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Login Error!')
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(3000)
                    );
                }
            );
        };
    }])


;