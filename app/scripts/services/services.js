'use strict';

angular.module('financeTracker')
    .constant("baseURL", "https://finance-tracker-back.herokuapp.com/api/")
    .constant("fixer", "https://api.fixer.io/latest?")

//Validates login logout and register of users also validates if an user can go to certain path if loggeed in with the validatePath Method
.factory('AuthFactory', ['$resource', 'baseURL', '$state', 'cookieFactory', function($resource, baseURL, $state, cookieFactory) {

    var authFact = {};
    var isAuthenticated = false;


    authFact.login = function() {
        return $resource(baseURL + "Customers/login", null, {
            'save': {
                method: 'POST'
            }
        });
    };

    authFact.logout = function() {
        return $resource(baseURL + "Customers/logout", null, {
            'save': {
                method: 'POST'
            }
        });
    };

    authFact.register = function() {
        return $resource(baseURL + "Customers", null, {
            'save': {
                method: 'POST'
            }
        });
    };

    authFact.isValidUser = function() {
        return isAuthenticated;
    };

    authFact.setValidUser = function(state) {
        isAuthenticated = state;
    };

    authFact.validatePath = function(state) {
        if (isAuthenticated || cookieFactory.getcookie('_st') != undefined) {
            $state.go(state);
            return true;
        } else {
            $state.go('landing');
            return false;
        }

    };

    return authFact;
}])

// Manages cookies storage, creation and deletion
.factory('cookieFactory', ['$resource', 'baseURL', '$cookies', function($resource, baseURL, $cookies) {

    var cookieFact = {};


    cookieFact.setcookie = function(key, value, options) {
        $cookies.put(key, value, options);
    };

    cookieFact.getcookie = function(key) {
        return $cookies.get(key);
    };

    cookieFact.unsetcookie = function(key) {
        $cookies.remove(key);
    };
    return cookieFact;
}])


.factory('glossaryFactory', ['$resource', 'baseURL', function($resource, baseURL) {

    var glosFact = {};

    //Gets the tips from the database
    glosFact.getTips = function() {
        return $resource(baseURL + "tips", null, {
            'get': {
                method: 'GET'
            }
        });
    };
    return glosFact;
}])

.factory('currencyFactory', ['$resource', 'baseURL', 'fixer', function($resource, baseURL, fixer) {

    var cureFact = {};

    //Gets the currencies from the database to be shown in the popular currencies title
    cureFact.getNamedCurs = function() {
        return $resource(baseURL + "currencies  ", null, {
            'get': {
                method: 'GET'
            }
        });
    };
    // Returns the rate between two currencies
    cureFact.fixerCurs = function() {
        return $resource(fixer + "base=:origin&symbols=:versus", null, {
            'get': {
                method: 'GET',
                params: {
                    origin: '@origin',
                    versus: '@versus'
                }
            }
        });
    };
    return cureFact;
}])

.factory('trackerFactory', ['$resource', 'baseURL', 'cookieFactory', function($resource, baseURL, cookieFactory) {

    var trackFact = {};
    var st = cookieFactory.getcookie('_st');

    //Gets the tracker from the selected account and current user from the database
    trackFact.getTrackers = function(accountId) {
        return $resource(baseURL + "trackers?filter=%7B%22where%22%3A%20%7B%22and%22%3A%5B%20%7B%22customerId%22%3A%20%22" + cookieFactory.getcookie('_ui') + "%22%7D%2C%20%7B%22accountsId%22%3A%20%22" + accountId + "%22%7D%5D%7D%7D&access_token=" + cookieFactory.getcookie('_st'), {}, {
            get: {
                method: 'GET'
            }
        });
    };
    // creates new tracker for the selected account and current user
    trackFact.postTrack = function() {
        return $resource(baseURL + "trackers", null, {
            'save': {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": st
                }
            }
        });
    };
    return trackFact;
}])

.factory('accountFactory', ['$resource', 'baseURL', 'cookieFactory', function($resource, baseURL, cookieFactory) {

    var st = cookieFactory.getcookie('_st');
    var ui = cookieFactory.getcookie('_ui');
    var accountFact = {};

    //Gets the accounts of the current user
    accountFact.getAccounts = function() {
        return $resource(baseURL + "accounts?filter=%7B%20%22where%22%3A%20%7B%22customerId%22%3A%22" + cookieFactory.getcookie('_ui') + "%22%7D%7D&access_token=" + cookieFactory.getcookie('_st'), {}, {
            get: {
                method: 'GET'
            }
        });
    };

    //Creates a new account for the current user
    accountFact.registerAccount = function() {
        return $resource(baseURL + "accounts", null, {
            'save': {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": cookieFactory.getcookie('_st')
                }
            }
        });
    };
    //Deletes selected account for the curren user
    accountFact.deleteAccount = function() {
        return $resource(baseURL + "accounts/:id", { id: "@id" }, {
            'delete': {
                method: 'DELETE',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": cookieFactory.getcookie('_st')
                }
            }
        });
    };


    return accountFact;
}])


;