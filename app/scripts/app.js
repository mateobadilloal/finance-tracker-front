'use strict';
//katex library is in charge of the mathematical notation display in the financial glossary view, nvd3 is in charge of displaying the graph
angular.module('financeTracker', ['ui.router', 'ngResource', 'ngMaterial', 'ngDialog', 'ngMessages', 'ngAnimate', 'angular-loading-bar', 'ngCookies', 'katex', 'nvd3'])
    .config(function($stateProvider, $urlRouterProvider, $mdThemingProvider, cfpLoadingBarProvider) {
        $stateProvider
        // route for the home page
            .state('landing', {
                url: '/',
                views: {
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'headerController'
                    },
                    'content@': {
                        templateUrl: 'views/landing.html',
                        controller: 'landingController'
                    },
                    'footer': {
                        templateUrl: 'views/footer.html',
                        controller: 'footerController'
                    },
                }

            })
            .state('landing.currency', {
                url: 'currency',
                views: {
                    'content@': {
                        templateUrl: 'views/currency.html',
                        controller: 'currencyController'
                    }
                }

            })
            .state('landing.glossary', {
                url: 'glossary',
                views: {
                    'content@': {
                        templateUrl: 'views/glossary.html',
                        controller: 'glossaryController'
                    }
                }

            })
            .state('landing.dashboard', {
                abstract: true,
                url: 'dashboard',
                views: {
                    'content@': {
                        templateUrl: 'views/dashboard.html',
                        controller: 'dashboardController'
                    }
                }


            })
            .state('landing.dashboard.tracker', {
                url: '/tracker',
                templateUrl: 'views/tracker.html',
                controller: 'trackerController'
            })
            .state('landing.dashboard.accounts', {
                url: '/accounts',
                templateUrl: 'views/accounts.html',
                controller: 'accountsController'
            })
            .state('login', {
                url: '/login',
                views: {
                    'content@': {
                        templateUrl: 'views/login.html',
                        controller: 'loginController'
                    }
                }

            })
            .state('register', {
                url: '/register',
                views: {
                    'content@': {
                        templateUrl: 'views/register.html',
                        controller: 'registerController'
                    }
                }

            })

        ;

        $urlRouterProvider.otherwise('/');
        cfpLoadingBarProvider.includeSpinner = false;


    });